package com.jackson.app

import android.app.Application
import android.util.Log

/**
 * @author: jackson liao
 * @createDate: 2021/12/2 17:47
 * @description:
 */
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.i("MainApp", "onCreate>>>>>>")
    }
}