package com.jackson.app.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.jackson.gradleapp.BuildConfig
import com.jackson.gradleapp.R

/**
 * @author: jackson liao
 * @createDate: 2021/12/2 17:51
 * @description:
 */
class SplashActivity : AppCompatActivity() {
    private companion object {
        private const val TAG = "SplashActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splash_activity)


        Log.i(TAG, BuildConfig.VERSION_NAME + "," + BuildConfig.VERSION_CODE)
    }
}