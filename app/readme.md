### 生产签名文件
```
    keytool -genkey -v -keystore demo.jks -alias jackson  -storepass 123456 -keypass a123456 -keyalg RSA -validity 14000

    备注:
    -keystore：设置生成的文件名称，包含后缀；
    -alias：设置别名
    -storepass：设置文件的密码
    -keypass：设置key的密码
    -keyalg：设置使用的加密算法，一般写RSA
    -validity：设置有效期，尽可能长啦
```

### jks迁移到行业标准格式PKCS12
```
keytool -importkeystore -srckeystore demo.jks -destkeystore demo.jks -deststoretype pkcs12

// 指定别名
keytool -importkeystore -srckeystore demo.jks -destkeystore demo.jks -destkeypass a123456 -deststoretype pkcs12
```

https://www.jianshu.com/p/eaae3c1e0796



### pgp 
https://zhuanlan.zhihu.com/p/359228319
https://xiaozhuanlan.com/topic/6174835029
https://stackoverflow.com/questions/27936119/gradle-uploadarchives-task-unable-to-read-secret-key
https://fullstackaction.github.io/pages/8ad3be/
```

gpg --list-keys

创建密钥：
gpg --full-gen-key

//控制台展示
Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
  (14) Existing key from card
Your selection? 1
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 4096
Requested keysize is 4096 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name:
Email address:
Comment:

然后弹出输入一个密码保护的，输入一个密码即可。

//导出pgp公钥
gpg --export-secret-keys -o secring.gpg


gpg --keyserver hkp://pool.sks-keyservers.net:11371 --send-keys A1D3A5E4
gpg --keyserver hkp://pgp.mit.edu:11371 --send-keys A1D3A5E4
gpg --keyserver hkp://keyserver.ubuntu.com:11371 --send-keys A1D3A5E4
　　
上传完毕可以通过下面命令进行查询看是否上传成功：
gpg --keyserver hkp://pool.sks-keyservers.net:11371 --recv-keys A1D3A5E4
gpg --keyserver hkp://pgp.mit.edu:11371 --recv-keys A1D3A5E4
gpg --keyserver hkp://keyserver.ubuntu.com:11371 --recv-keys A1D3A5E4

```

### mavenCentral
查询库：https://search.maven.org/

