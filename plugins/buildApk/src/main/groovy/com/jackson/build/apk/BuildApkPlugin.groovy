package com.jackson.build.apk

import com.android.build.gradle.AppExtension
import com.jackson.build.apk.core.BuildApkTasks
import com.jackson.build.apk.core.Constants
import com.jackson.build.apk.utils.Logger
import com.jackson.build.apk.vo.BuildApkVo
import com.jackson.build.apk.vo.SignApkVo
import org.apache.http.util.TextUtils
import org.gradle.api.Plugin
import org.gradle.api.Project

class BuildApkPlugin implements Plugin<Project> {

    String rootPath = ""
    String debugRootPath = ""
    private SignApkVo releaseSign = new SignApkVo()

    @Override
    void apply(Project project) {
//        Logger.println("BuildApkPlugin start.....$project")
        def buildApkVo = project.getExtensions().create(Constants.BUILD_APK_NAME, BuildApkVo)
        project.afterEvaluate {
            checkBuildApk(project, buildApkVo)
//            Logger.println("BuildApkPlugin config:\n $buildApkVo")
            def android = project.getExtensions().getByType(AppExtension)
            initApkOutDir(android, buildApkVo)
            initSignInfo(android)

            new BuildApkTasks(project, rootPath, buildApkVo, releaseSign).dependsTasks()
        }
    }

    private void checkBuildApk(Project project, BuildApkVo buildApkVo) {
//        Logger.println(project.projectDir.absolutePath)
//        Logger.println(project.rootDir.absolutePath)

        def buildChannel = buildApkVo.buildChannel
        if (buildChannel != null && buildChannel.enable) {
            if (TextUtils.isEmpty(buildChannel.outPutPath)) {
//                buildChannel.outPutPath = new File(project.projectDir, "channels").absolutePath
                buildChannel.outPutPath = "${project.projectDir.absolutePath}${File.separator}channels"
            }
            if (TextUtils.isEmpty(buildChannel.channelPath)) {
//                buildChannel.channelPath = new File(project.projectDir, "channel").absolutePath
                buildChannel.channelPath = "${project.projectDir.absolutePath}${File.separator}channel"
            }
        }
    }

    private void initApkOutDir(AppExtension android, BuildApkVo buildApkVo) {
        android.applicationVariants.all { variant ->
            if (buildApkVo.gradleVersion == 4) {
                variant.outputs.all { output ->
                Logger.println("release2--> ${output.outputFile}")
                Logger.println("release3--> ${output.outputFile.name}")
                    if (variant.buildType.name == "release") {
                        rootPath = output.outputFile
//                        rootPath = "${apkRootPath}${File.separator}${output.outputFile.name}"
//                    Logger.println("release--> $rootPath")
                    } else if (variant.buildType.name == "debug") {
                        debugRootPath = output.outputFile
//                        debugRootPath = "${apkRootPath}${File.separator}${output.outputFile.name}"
//                    Logger.println("debug--> $debugRootPath")
                    }
                }
            } else {
                String apkRootPath = variant.packageApplicationProvider.get().outputDirectory
//            Logger.println("apkRootPath=$apkRootPath")
                variant.outputs.all { output ->
//                Logger.println("release2--> ${output.outputFile}")
                    if (variant.buildType.name == "release") {

                        rootPath = "${apkRootPath}${File.separator}${output.outputFile.name}"
//                    Logger.println("release--> $rootPath")
                    } else if (variant.buildType.name == "debug") {
                        debugRootPath = "${apkRootPath}${File.separator}${output.outputFile.name}"
//                    Logger.println("debug--> $debugRootPath")
                    }
                }
            }
        }
//        def versionName = android.defaultConfig.versionName
//        Logger.println("$rootPath,$debugRootPath ,$versionName")
    }


    private void initSignInfo(android) {
        android.signingConfigs.forEach {
            if (it.name == "release") {
                releaseSign.signPassword = it.storePassword
                releaseSign.signKeyAlias = it.keyAlias
                releaseSign.signKeyPwd = it.keyPassword
                releaseSign.signPath = it.storeFile.absolutePath
                Logger.info("release sign: $releaseSign")
            }
        }
    }


}