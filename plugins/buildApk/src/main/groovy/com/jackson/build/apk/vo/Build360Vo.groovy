package com.jackson.build.apk.vo

/**
 * @author: jackson liao
 * @createDate: 2021/11/2 11:19
 * @description: 360 加固配置
 */
class Build360Vo {
    Boolean enable = false
    String account = ""
    String password = ""
    // 增值服务配置：如 -x86 -analyse -crashlog -data -filecheck -dex2c
    String config = ""

    /**jar 加固路径需要自己赋值*/
    String jarPath = ""
    /**因为360加固必须使用他们提供的java来进行上传加固，所以必须配置*/
    String javaPath = ""


    @Override
    public String toString() {
        return "Build360Vo{" +
                "enable=" + enable +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", config='" + config + '\'' +
                ", jarPath='" + jarPath + '\'' +
                ", javaPath='" + javaPath + '\'' +
                '}';
    }
}