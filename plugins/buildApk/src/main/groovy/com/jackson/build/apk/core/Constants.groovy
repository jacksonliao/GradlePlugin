package com.jackson.build.apk.core

/**
 * @author: jackson liao
 * @createDate: 2021/11/2 11:36
 * @description: 常量
 */
interface Constants {
    String BUILD_APK_NAME = "buildApk"

    String DEFAULT_APK_FAIL = "app-{buildTime}-{channel}-{versionName}_{versionCode}.apk"

    //360加固命令行文档： https://jiagu.360.cn/#/global/help/164
    String LOGIN_CMD = "%1s -jar %2s -login %3s %4s"
    String IMPORT_KEY_CMD = "%1s -jar %2s -importsign %3s %4s %5s %6s"
    String CHECK_SIGN_INFO_CMD = "%1s -jar %2s -showsign"
    String INIT_JIAGU_SERVICE_CMD = "%1s -jar %2s -config %3s"
    String INIT_JIAGU_SHOW_CONFIG_CMD = "%1s -jar %2s -showconfig"
}