package com.jackson.beetle.ext

/**
 *
 * desc:   基础配置
 * author: 行走的老者
 * date: 2020-01-09 16:41
 */
class BaseExt {
    String name
    String applicationId
    String applicationName
    String mainActivity

    BaseExt(String name) {
        this.name = name
    }


    @Override
    public String toString() {
        return "BaseExt{" +
                "name='" + name + '\'' +
                ", applicationId='" + applicationId + '\'' +
                ", applicationName='" + applicationName + '\'' +
                ", mainActivity='" + mainActivity + '\'' +
                '}';
    }
}
