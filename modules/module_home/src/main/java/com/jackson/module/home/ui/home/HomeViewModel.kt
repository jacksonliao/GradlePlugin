package com.jackson.module.home.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.jackson.module.home.vo.TestBean
import java.util.ArrayList
import java.util.HashMap

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {

        val innerBean = TestBean()
        innerBean.age = 18
        innerBean.name = "test1"
        innerBean.sex = "boy"
        innerBean.array = arrayOfNulls<String>(2)
        innerBean.array[0]  = "array1"
        innerBean.array[1] = "array2"
        innerBean.list = ArrayList()
        innerBean.list.add("list1")
        innerBean.list.add("list2")
        innerBean.map = HashMap()
        innerBean.map.put("map1", "map1")
        innerBean.map.put("map2", "map2")

        val testBean = TestBean()
        testBean.age = 20
        testBean.name = "testBean"
        testBean.sex = "girl"
        testBean.bean = innerBean

        var testStr = Gson().toJson(testBean)
//        Log.d("test", "str: $testStr")
        testStr =
            "{\"age\":20,\"is_success\":true,\"sex\":\"girl\",\"bean\":{\"age\":18.8,\"var1\":\"a\",\"var2\":\"b\",\"var3\":[],\"is_success\":1,\"array\":{},\"list\":[\"list1\",\"list2\"],\"sex\":\"boy\",\"map\":\"\",\"name\":\"test1\"},\"name\":\"test2\"}"
//                testStr = "{\"age\":20,\"is_success\":true,\"sex\":\"girl\",\"bean\":{\"age\":18.8,\"var1\":\"a\",\"var2\":\"b\",\"var3\":[],\"is_success\":1,\"array\":{},\"list\":[\"list1\",\"list2\"],\"sex\":\"boy\",\"map\":\"\",\"name\":\"test1\"},\"name\":[12]}";

        //                testStr = "{\"age\":20,\"is_success\":true,\"sex\":\"girl\",\"bean\":{\"age\":18.8,\"var1\":\"a\",\"var2\":\"b\",\"var3\":[],\"is_success\":1,\"array\":{},\"list\":[\"list1\",\"list2\"],\"sex\":\"boy\",\"map\":\"\",\"name\":\"test1\"},\"name\":[12]}";
//        try {
        val resultBean: TestBean = Gson().fromJson(testStr, TestBean::class.java)
        Log.d("test", "result bean: $resultBean")
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }

        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text
}